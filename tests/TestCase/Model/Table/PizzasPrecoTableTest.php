<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PizzasPrecoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PizzasPrecoTable Test Case
 */
class PizzasPrecoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PizzasPrecoTable
     */
    public $PizzasPreco;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pizzas_preco',
        'app.pizzas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PizzasPreco') ? [] : ['className' => 'App\Model\Table\PizzasPrecoTable'];
        $this->PizzasPreco = TableRegistry::get('PizzasPreco', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PizzasPreco);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
