<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PizzasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PizzasTable Test Case
 */
class PizzasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PizzasTable
     */
    public $Pizzas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pizzas',
        'app.pizzas_preco'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pizzas') ? [] : ['className' => 'App\Model\Table\PizzasTable'];
        $this->Pizzas = TableRegistry::get('Pizzas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pizzas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
