<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PedidoFixture
 *
 */
class PedidoFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'pedido';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'cliente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pizza_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pizzas_preco_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_pedido' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'clientes_pk' => ['type' => 'index', 'columns' => ['cliente_id'], 'length' => []],
            'pizza_fk' => ['type' => 'index', 'columns' => ['pizza_id'], 'length' => []],
            'pizza_preco_fk' => ['type' => 'index', 'columns' => ['pizzas_preco_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'pedido_ibfk_1' => ['type' => 'foreign', 'columns' => ['cliente_id'], 'references' => ['cliente', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'pedido_ibfk_2' => ['type' => 'foreign', 'columns' => ['pizza_id'], 'references' => ['pizzas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'pedido_ibfk_3' => ['type' => 'foreign', 'columns' => ['pizzas_preco_id'], 'references' => ['pizzas_preco', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cliente_id' => 1,
            'pizza_id' => 1,
            'pizzas_preco_id' => 1,
            'data_pedido' => '2016-10-13'
        ],
    ];
}
