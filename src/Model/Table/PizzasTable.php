<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pizzas Model
 *
 * @property \Cake\ORM\Association\HasMany $PizzasPreco
 *
 * @method \App\Model\Entity\Pizza get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pizza newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pizza[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pizza|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pizza patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pizza[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pizza findOrCreate($search, callable $callback = null)
 */
class PizzasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pizzas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('PizzasPreco', [
            'foreignKey' => 'pizza_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }
}
