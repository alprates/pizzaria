<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pedido Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cliente
 * @property \Cake\ORM\Association\BelongsTo $Pizzas
 * @property \Cake\ORM\Association\BelongsTo $PizzasPreco
 *
 * @method \App\Model\Entity\Pedido get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pedido newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pedido[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pedido|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pedido patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pedido[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pedido findOrCreate($search, callable $callback = null)
 */
class PedidoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pedido');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cliente', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('Pizzas', [
            'foreignKey' => 'pizza_id'
        ]);
        $this->belongsTo('PizzasPreco', [
            'foreignKey' => 'pizzas_preco_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('data_pedido')
            ->allowEmpty('data_pedido');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Cliente'));
        $rules->add($rules->existsIn(['pizza_id'], 'Pizzas'));
        $rules->add($rules->existsIn(['pizzas_preco_id'], 'PizzasPreco'));

        return $rules;
    }
}
