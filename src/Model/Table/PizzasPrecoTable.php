<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PizzasPreco Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Pizzas
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Pedido
 *
 * @method \App\Model\Entity\PizzasPreco get($primaryKey, $options = [])
 * @method \App\Model\Entity\PizzasPreco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PizzasPreco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PizzasPreco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PizzasPreco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PizzasPreco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PizzasPreco findOrCreate($search, callable $callback = null)
 */
class PizzasPrecoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pizzas_preco');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Pizzas', [
            'foreignKey' => 'pizza_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Pedido', [
            'foreignKey' => 'pizzas_preco_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('preco')
            ->requirePresence('preco', 'create')
            ->notEmpty('preco');

        $validator
            ->requirePresence('tamanho', 'create')
            ->notEmpty('tamanho');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pizza_id'], 'Pizzas'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
