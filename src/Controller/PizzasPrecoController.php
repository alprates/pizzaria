<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PizzasPreco Controller
 *
 * @property \App\Model\Table\PizzasPrecoTable $PizzasPreco
 */
class PizzasPrecoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Pizzas']
        ];
        $pizzasPreco = $this->paginate($this->PizzasPreco);

        $this->PizzasPreco->user_id = $this->Auth->user('id');

        $this->set(compact('pizzasPreco'));
        $this->set('_serialize', ['pizzasPreco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $pizzasPreco = $this->PizzasPreco->newEntity();
        if ($this->request->is('post')) {
            $pizzasPreco = $this->PizzasPreco->patchEntity($pizzasPreco, $this->request->data);
            if ($this->PizzasPreco->save($pizzasPreco)) {
                $this->Flash->success(__('The pizzas preco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pizzas preco could not be saved. Please, try again.'));
            }
        }
        $pizzas = $this->PizzasPreco->Pizzas->find('all', ['limit' => 200]);
        $this->set(compact('pizzasPreco', 'pizzas'));
        $this->set('_serialize', ['pizzasPreco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pizzas Preco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pizzasPreco = $this->PizzasPreco->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $pizzasPreco = $this->PizzasPreco->patchEntity($pizzasPreco, $this->request->data);
            if ($this->PizzasPreco->save($pizzasPreco)) {
                $this->Flash->success(__('The pizzas preco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pizzas preco could not be saved. Please, try again.'));
            }
        }
        $pizzas = $this->PizzasPreco->Pizzas->find('all', ['limit' => 200]);
      
        $this->set(compact('pizzasPreco', 'pizzas'));
        $this->set('_serialize', ['pizzasPreco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pizzas Preco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pizzasPreco = $this->PizzasPreco->get($id);
        if ($this->PizzasPreco->delete($pizzasPreco)) {
            $this->Flash->success(__('The pizzas preco has been deleted.'));
        } else {
            $this->Flash->error(__('The pizzas preco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
