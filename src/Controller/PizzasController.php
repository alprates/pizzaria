<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pizzas Controller
 *
 * @property \App\Model\Table\PizzasTable $Pizzas
 */
class PizzasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pizzas = $this->paginate($this->Pizzas);

        $this->set(compact('pizzas'));
        $this->set('_serialize', ['pizzas']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pizza = $this->Pizzas->newEntity();
        if ($this->request->is('post')) {
            $pizza = $this->Pizzas->patchEntity($pizza, $this->request->data);
            if ($this->Pizzas->save($pizza)) {
                $this->Flash->success(__('The pizza has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pizza could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pizza'));
        $this->set('_serialize', ['pizza']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pizza id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pizza = $this->Pizzas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pizza = $this->Pizzas->patchEntity($pizza, $this->request->data);
            if ($this->Pizzas->save($pizza)) {
                $this->Flash->success(__('The pizza has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pizza could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pizza'));
        $this->set('_serialize', ['pizza']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pizza id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pizza = $this->Pizzas->get($id);
        if ($this->Pizzas->delete($pizza)) {
            $this->Flash->success(__('The pizza has been deleted.'));
        } else {
            $this->Flash->error(__('The pizza could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
