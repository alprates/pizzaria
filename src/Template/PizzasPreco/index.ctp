<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pizzas Preco'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas'), ['controller' => 'Pizzas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizza'), ['controller' => 'Pizzas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pizzasPreco index large-9 medium-8 columns content">
    <h3><?= __('Pizzas Preco') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pizza_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('preco') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pizzasPreco as $pizzasPreco): ?>
            <tr>
                <td><?= $this->Number->format($pizzasPreco->id) ?></td>
                <td><?= $pizzasPreco->has('pizza') ? $pizzasPreco->pizza->nome : '' ?></td>
                <td><?= $this->Number->format($pizzasPreco->preco) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pizzasPreco->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pizzasPreco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pizzasPreco->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
