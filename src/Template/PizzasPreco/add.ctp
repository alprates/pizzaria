<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pizzas Preco'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas'), ['controller' => 'Pizzas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizza'), ['controller' => 'Pizzas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pizzasPreco form large-9 medium-8 columns content">
    <?= $this->Form->create($pizzasPreco) ?>
    <fieldset>
        <legend><?= __('Add Pizzas Preco') ?></legend>
        <label for="pizza_id"><strong>Pizzas:</strong></label>
        <select name="pizza_id" id="pizza_id">
          <option selected="selected">Selecione</option>
          <?php
            foreach($pizzas as $p) { ?>
              <option value="<?= $p['id'] ?>"><?= $p['nome'] ?></option>
          <?php
            } ?>
        </select>
        <?php
            echo $this->Form->input('preco');
            echo $this->Form->input('tamanho', ['options' => array('Pequeno','Medio','Grande'), 'empty' => false]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>