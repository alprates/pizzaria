<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min'); ?>
    <?=  $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'); ?>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>
<body class="home">
        <div class="center boxtelefone">
            Ligue agora e peça Delivery! <br />1234-4567
 
      </div>
    <header>
        <div class="header-image">
            <?= $this->Html->image('logo.png', array('width' => '250')) ?>
            <h1>Pizzaria Prates</h1>
        </div>


    </div>
  </div>


    </header>
    <div id="content">
        <div class="row">
            <div class="columns large-12 ctp-warning checks">
                <center><strong>Clique <?= $this->Html->link(__('aqui'), ['controller' => 'Pedido','action' => 'index']) ?> para fazer o pedido.
                </strong></center>
            </div>
            <?= $this->Html->image('banner.jpg') ?>


            <div id="url-rewriting-warning" class="columns large-12 url-rewriting checks">
                <p class="problem">URL rewriting is not properly configured on your server.</p>
                <p>
                    1) <a target="_blank" href="http://book.cakephp.org/3.0/en/installation.html#url-rewriting">Help me configure it</a>
                </p>
                <p>
                    2) <a target="_blank" href="http://book.cakephp.org/3.0/en/development/configuration.html#general-configuration">I don't / can't use URL rewriting</a>
                </p>
            </div>

            <div class="columns large-12 checks">
                <p class="success">Melhor preco da cidade.</p>
                <p class="success">Programa de fidelidade.</p>
                <p class="success">Entrega rapida.</p>
                <p class="success">Confira nossas ofertas.</p>
            </div>
    </div>

<footer>    
      </div>
      <div class="col-md-7 bagcblue">
        <div class="heading6">Endereco:</div>
            
      
         <ul class="social">
          <p></p>
          <li><a target="_blank" href="https://www.facebook.com/imageeditingservice"><i class="fa fa-facebook"></i></a></li>
          <li><a target="_blank" href="https://twitter.com/imageeditingi2v"><i class="fa fa-twitter"></i></a></li>
          <li><a target="_blank" href="https://www.linkedin.com/company/image2vectorgraphicsindia"><i class="fa fa-linkedin"></i></a></li>
          <li><a target="_blank" href="https://plus.google.com/+Image2vectorgraphicsIndia9"><i class="fa fa-google-plus"></i></a></li>
          <li><a target="_blank" href="https://www.pinterest.com/imageediting/"><i class="fa fa-pinterest"></i></a></li>
        </ul>  
        <small>Rua Osorio equina com Tamaios, 121</small><br />
            <small>Telefone 1234-4567</small><br />
            <small>© Copyright 2016, Prates Pizaria</small>
        <br />
      </div>
    </div>
  </div>

</footer>

</body>
</html>
