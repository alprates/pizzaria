<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pedido'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cliente'), ['controller' => 'Cliente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cliente'), ['controller' => 'Cliente', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas'), ['controller' => 'Pizzas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizza'), ['controller' => 'Pizzas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pedido index large-9 medium-8 columns content">
    <h3><?= __('Pedido') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table">
        <thead class="thead-inverse">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cliente_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pizza_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Tamanho') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Preco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_pedido') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedido as $pedido): ?>
            <tr>
                <td><?= $this->Number->format($pedido->id) ?></td>
                <td><?= $pedido->has('cliente') ? $pedido->cliente->nome : '' ?></td>
                <td><?= $pedido->has('pizza') ? $pedido->pizza->nome : '' ?></td>
                <td><?= $pedido->has('pizzas_preco') ? $pedido->pizzas_preco->tamanho : '' ?></td>
                <td><?= $pedido->has('pizzas_preco') ? $pedido->pizzas_preco->preco : '' ?></td>
                <td><?= h($pedido->data_pedido) ?></td>
                <td class="actions">
                     <?= $this->Html->link(__('View'), ['action' => 'view', $pedido->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pedido->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pedido->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pedido->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>