<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pedido'), ['action' => 'edit', $pedido->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pedido'), ['action' => 'delete', $pedido->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pedido->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pedido'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pedido'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cliente'), ['controller' => 'Cliente', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cliente'), ['controller' => 'Cliente', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pizzas'), ['controller' => 'Pizzas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pizza'), ['controller' => 'Pizzas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pedido view large-9 medium-8 columns content">
    <h3><?= h($pedido->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= $pedido->cliente->nome; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pizza') ?></th>
            <td><?= $pedido->pizza->nome; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tamanho') ?></th>
            <td><?= $pedido->pizzas_preco->tamanho; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Preco') ?></th>
            <td><?= $pedido->pizzas_preco->preco; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Pedido') ?></th>
            <td><?= h($pedido->data_pedido) ?></td>
        </tr>
    </table>
</div>
