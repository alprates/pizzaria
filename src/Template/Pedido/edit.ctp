<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pedido->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pedido->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pedido'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cliente'), ['controller' => 'Cliente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cliente'), ['controller' => 'Cliente', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas'), ['controller' => 'Pizzas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizza'), ['controller' => 'Pizzas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pedido form large-9 medium-8 columns content">
    <?= $this->Form->create($pedido) ?>
    <fieldset>
        <legend><?= __('Edit Pedido') ?></legend>
        Cliente:
        <select name="cliente_id" id="cliente_id">
          <option selected="selected">Selecione</option>
          <?php
            foreach($cliente as $c) { ?>
              <option value="<?= $c['id'] ?>"><?= $c['nome'] ?></option>
          <?php
            } ?>
        </select>
        Pizza:
        <select name="pizza_id" id="pizza_id">
          <option selected="selected">Selecione</option>
          <?php
            foreach($pizzas as $p) { ?>
              <option value="<?= $p['id'] ?>"><?= $p['nome'] ?></option>
          <?php
            } ?>
        </select>
        <?php
            echo $this->Form->input('pizzas_preco_id', ['options' => $pizzasPreco, 'empty' => true]);
            echo $this->Form->input('data_pedido', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
