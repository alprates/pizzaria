<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pizzas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pizzas Preco'), ['controller' => 'PizzasPreco', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pizzas form large-9 medium-8 columns content">
    <?= $this->Form->create($pizza) ?>
    <fieldset>
        <legend><?= __('Add Pizza') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->input('path', ['type' => 'file']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
